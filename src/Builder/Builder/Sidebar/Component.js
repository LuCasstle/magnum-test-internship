import * as React from "react";
import { useDrag } from "react-dnd";

import { Tag } from "react-bulma-components";

export default function SidebarComponent({ componentId }) {
  const [{ dragging }, dragRef] = useDrag(
    () => ({
      type: "COMPONENT",
      item: { id: componentId, type: "COMPONENT" },
      collect: (monitor) => ({
        dragging: monitor.isDragging(),
      }),
    }),
    []
  );

  return (
    <div style={{ opacity: dragging ? 0.5 : 1 }} ref={dragRef}>
      <Tag color="primary">{componentId}</Tag>
    </div>
  );
}
