export const COLUMN_SIZES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
export const LAYOUT_TYPES = [
  [6, 6],
  [4, 4, 4],
  [3, 3, 3, 3],
  [3, 6, 3],
];
