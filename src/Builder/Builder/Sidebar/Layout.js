import * as React from "react";
import { useDrag } from "react-dnd";

import { Tag } from "react-bulma-components";

export default function SidebarLayouts({ layoutType }) {
  const [{ dragging }, dragRef] = useDrag(
    () => ({
      type: "LAYOUT_TYPE",
      item: { id: JSON.stringify(layoutType), type: "LAYOUT_TYPE" },
      collect: (monitor) => ({
        dragging: monitor.isDragging(),
      }),
    }),
    []
  );

  return (
    <div style={{ opacity: dragging ? 0.5 : 1 }} ref={dragRef}>
      <Tag color="dark">{JSON.stringify(layoutType)}</Tag>
    </div>
  );
}
