import * as React from "react";
import COMPONENT_MAP from "../../components";
import { LAYOUT_TYPES } from "./constants";
import SidebarComponent from "./Component";
import SidebarLayout from "./Layout";

export default function Sidebar() {
  console.log(COMPONENT_MAP);
  return (
    <div className="app-content">
      {Object.keys(COMPONENT_MAP).map((componentId) => (
        <SidebarComponent key={componentId} componentId={componentId} />
      ))}

      {LAYOUT_TYPES.map((layoutType) => (
        <SidebarLayout
          key={JSON.stringify(layoutType)}
          layoutType={layoutType}
        />
      ))}
    </div>
  );
}
