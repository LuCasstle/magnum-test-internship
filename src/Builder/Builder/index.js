import * as React from "react";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { nanoid } from "nanoid";

import { Columns } from "react-bulma-components";
import Sidebar from "./Sidebar";
import Page from "./Page";

export default function Builder({ layout, setLayout }) {
  React.useEffect(() => {
    localStorage.setItem("layout", JSON.stringify(layout));
  }, [layout]);

  const addRow = React.useCallback(() => {
    setLayout((previousLayout) => {
      const newLayout = [
        ...previousLayout,
        {
          id: nanoid(),
          components: [],
          columns: [],
        },
      ];

      return newLayout;
    });
  }, [setLayout]);

  const deleteRow = React.useCallback(
    (deletedRow) =>
      setLayout((previousLayout) => {
        const rowIndex = previousLayout.findIndex(
          (row) => row.id === deletedRow.id
        );

        const newLayout = [
          ...previousLayout.slice(0, rowIndex),
          ...previousLayout.slice(rowIndex + 1),
        ];

        return newLayout;
      }),
    [setLayout]
  );

  const updateRow = React.useCallback(
    (updatedRow) =>
      setLayout((previousLayout) => {
        const rowIndex = previousLayout.findIndex(
          (row) => row.id === updatedRow.id
        );

        const newLayout = [
          ...previousLayout.slice(0, rowIndex),
          updatedRow,
          ...previousLayout.slice(rowIndex + 1),
        ];

        return newLayout;
      }),
    [setLayout]
  );

  const reset = React.useCallback(() => setLayout([]), [setLayout]);

  return (
    <DndProvider backend={HTML5Backend}>
      <Columns>
        <Columns.Column size={2}>
          <Sidebar />
        </Columns.Column>

        <Columns.Column size={10}>
          <Page
            layout={layout}
            addRow={addRow}
            deleteRow={deleteRow}
            updateRow={updateRow}
            reset={reset}
          />
        </Columns.Column>
      </Columns>
    </DndProvider>
  );
}
