import * as React from "react";
import { useDrop } from "react-dnd";
import { nanoid } from "nanoid";

import { Panel } from "react-bulma-components";
import Component from "./Component";

export default function BuilderPageColumn({ column, onChange }) {
  const { id, components } = column;

  const [, dropRef] = useDrop(
    () => ({
      accept: "COMPONENT",
      drop: (component) => {
        onChange({
          ...column,
          components: [
            ...components,
            {
              ...component,
              componentId: component.id,
              id: nanoid(),
              options: {},
            },
          ],
        });
      },
    }),
    [onChange, column]
  );

  const updateComponent = React.useCallback(
    (updatedComponent) => {
      const updatedComponentIndex = column.components.findIndex(
        (component) => component.id === updatedComponent.id
      );

      onChange({
        ...column,
        components: [
          ...column.components.slice(0, updatedComponentIndex),
          updatedComponent,
          ...column.components.slice(updatedComponentIndex + 1),
        ],
      });
    },
    [column, onChange]
  );

  const removeComponent = React.useCallback(
    (component) => {
      const componentIndex = column.components.findIndex(
        (comp) => comp.id === component.id
      );

      onChange({
        ...column,
        components: [
          ...column.components.slice(0, componentIndex),
          ...column.components.slice(componentIndex + 1),
        ],
      });
    },
    [column, onChange]
  );

  return (
    <div ref={dropRef}>
      <Panel size="small" color="warning">
        <Panel.Header>
          <p>Column avec id {id.slice(0, 3)}</p>
        </Panel.Header>
        <div style={{ padding: 10 }}>
          {components.map((component) => (
            <Component
              component={component}
              key={component.id}
              onChange={updateComponent}
              onRemove={removeComponent}
            />
          ))}
        </div>
      </Panel>
    </div>
  );
}
