import * as React from "react";

import { Button, Level } from "react-bulma-components";
import Row from "./Row";

export default function Page({ layout, addRow, updateRow, deleteRow, reset }) {
  return (
    <div className="app-content builder-page">
      {layout.map((row) => (
        <Row key={row.id} row={row} onChange={updateRow} onDelete={deleteRow} />
      ))}

      <Level>
        <Button onClick={addRow} style={{ marginTop: 10 }}>
          Add to row
        </Button>
        <Button onClick={reset}>Reset</Button>
      </Level>
    </div>
  );
}
