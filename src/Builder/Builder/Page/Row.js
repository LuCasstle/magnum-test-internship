import * as React from "react";
import { useDrop } from "react-dnd";
import { nanoid } from "nanoid";

import { Panel, Columns as BulmaColumns } from "react-bulma-components";
import Component from "./Component";
import Column from "./Column";

function Row({ row, onChange }) {
  const { components, columns, id } = row;

  const [, dropRef] = useDrop(
    () => ({
      accept: ["COMPONENT", "LAYOUT_TYPE"],
      drop: (droppedElement) => {
        if (droppedElement.type === "COMPONENT" && !row.columns.length) {
          onChange({
            ...row,
            components: [
              ...components,
              {
                ...droppedElement,
                componentId: droppedElement.id,
                id: nanoid(),
                options: {},
              },
            ],
          });
        } else if (droppedElement.type === "LAYOUT_TYPE") {
          onChange({
            ...row,
            components: [],
            columns: JSON.parse(droppedElement.id).map(
              (columnSize, columnIndex) => {
                let column = {
                  id: nanoid(),
                  size: columnSize,
                  components: [],
                };

                if (columnIndex === 0)
                  column = { ...column, components: row.components };

                return column;
              }
            ),
          });
        }
      },
      canDrop: (component) => !row.columns.length,
    }),
    [onChange, row]
  );

  const updateColumn = React.useCallback(
    (updatedColumn) => {
      const updatedColumnIndex = row.columns.findIndex(
        (column) => column.id === updatedColumn.id
      );

      onChange({
        ...row,
        columns: [
          ...row.columns.slice(0, updatedColumnIndex),
          updatedColumn,
          ...row.columns.slice(updatedColumnIndex + 1),
        ],
      });
    },
    [row, onChange]
  );

  const updateComponent = React.useCallback(
    (updatedComponent) => {
      const updatedComponentIndex = row.components.findIndex(
        (component) => component.id === updatedComponent.id
      );

      onChange({
        ...row,
        components: [
          ...row.components.slice(0, updatedComponentIndex),
          updatedComponent,
          ...row.components.slice(updatedComponentIndex + 1),
        ],
      });
    },
    [row, onChange]
  );

  const removeComponent = React.useCallback(
    (component) => {
      const componentIndex = row.components.findIndex(
        (comp) => comp.id === component.id
      );

      onChange({
        ...row,
        components: [
          ...row.components.slice(0, componentIndex),
          ...row.components.slice(componentIndex + 1),
        ],
      });
    },
    [row, onChange]
  );

  return (
    <div ref={dropRef} className="row" style={{ marginBottom: 40 }}>
      <Panel color="primary">
        <Panel.Header>
          <p>Row avec id {id.slice(0, 3)}</p>
        </Panel.Header>
        <div style={{ padding: 10 }}>
          {components.map((component) => (
            <Component
              component={component}
              key={component.id}
              onChange={updateComponent}
              onRemove={removeComponent}
            />
          ))}

          {columns.length > 0 && (
            <BulmaColumns>
              {columns.map((column) => (
                <BulmaColumns.Column key={column.id} size={column.size}>
                  <Column column={column} onChange={updateColumn} />
                </BulmaColumns.Column>
              ))}
            </BulmaColumns>
          )}
        </div>
      </Panel>
    </div>
  );
}

export default React.memo(Row);
