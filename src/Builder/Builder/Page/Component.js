import * as React from "react";
import { fromPairs } from "lodash";

import COMPONENT_MAP from "../../components";

import { Panel, Modal, Button, Form } from "react-bulma-components";

export default function BuilderPageComponent({
  onChange,
  component,
  onRemove,
}) {
  const { id, componentId, options } = component;
  const optionSchema = COMPONENT_MAP[componentId].options;
  const [editModalOpen, openEditModal] = React.useState(false);
  const [editedOptions, setOptions] = React.useState(
    fromPairs(
      Object.keys(optionSchema).map((optionName) => [
        optionName,
        options[optionName] || "",
      ])
    )
  );

  return (
    <Panel color="danger">
      <Panel.Header>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div>{`${componentId}-${id.slice(0, 3)}`}</div>
          <div>
            <Button onClick={() => openEditModal(true)} size="small">
              <span class="icon">
                <i class="fas fa-edit"></i>
              </span>
            </Button>
            <Button
              onClick={() => onRemove(component)}
              size="small"
              color="danger"
            >
              <span class="icon">
                <i class="fas fa-times"></i>
              </span>
            </Button>
          </div>
        </div>

        <Modal onClose={() => openEditModal(false)} show={editModalOpen}>
          <Modal.Card>
            <Modal.Card.Header>
              <Modal.Card.Title>
                Options du composant {componentId}
              </Modal.Card.Title>
            </Modal.Card.Header>
            <Modal.Card.Body>
              {Object.keys(editedOptions).map((optionName) => {
                const editedOption = editedOptions[optionName];

                return (
                  <Form.Field>
                    <Form.Label>{optionName}</Form.Label>
                    <Form.Control>
                      <Form.Input
                        color="success"
                        value={editedOption}
                        type="text"
                        onChange={(e) =>
                          setOptions({
                            ...editedOptions,
                            [optionName]: e.target.value,
                          })
                        }
                      />
                    </Form.Control>
                  </Form.Field>
                );
              })}
            </Modal.Card.Body>
            <Modal.Card.Footer hasAddons style={{ justifyContent: "flex-end" }}>
              <Button onClick={() => openEditModal(false)}>Annuler</Button>
              <Button
                color="success"
                onClick={() => {
                  onChange({ ...component, options: editedOptions });
                  openEditModal(false);
                }}
              >
                Enregistrer
              </Button>
            </Modal.Card.Footer>
          </Modal.Card>
        </Modal>
      </Panel.Header>
    </Panel>
  );
}
