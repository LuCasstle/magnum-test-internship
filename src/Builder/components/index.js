import { fromPairs } from "lodash";

import * as Menu from "./Menu";
import * as Paragraph from "./Paragraph";
import * as Title from "./Title";
import * as Image from "./Image";

const COMPONENT_MAP = fromPairs(
  [Menu, Paragraph, Title, Image].map((compModule) => [
    compModule.ID,
    {
      Component: compModule.Component,
      options: compModule.OPTIONS,
      id: compModule.ID,
      category: compModule.CATEGORY,
    },
  ])
);

console.log(COMPONENT_MAP);
export default COMPONENT_MAP;
