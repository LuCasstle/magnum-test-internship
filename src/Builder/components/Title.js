export const OPTIONS = {
  content: {
    label: "content",
    type: "string",
  },
};

export const ID = "Title";
export const CATEGORY = "Contenu";

function Title({ options }) {
  const { content } = options;
  return <div>{content || "Empty title, no content provided"}</div>;
}

export const Component = Title;
