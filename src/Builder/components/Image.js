export const OPTIONS = {
  src: {
    label: "src",
    type: "string",
  },
};

export const ID = "Image";
export const CATEGORY = "Content";

function Image({ options }) {
  const { src } = options;
  if (!src) return "Empty image, no source provided";
  return <img src={src} alt="provided" />;
}

export const Component = Image;
