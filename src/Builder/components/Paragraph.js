export const OPTIONS = {
  content: {
    label: "content",
    type: "string",
  },
};

export const ID = "paragraph";
export const CATEGORY = "content";

function Paragraph({ options }) {
  const { content } = options;
  return <div>{content || "Empty paragraph, no content provided"}</div>;
}

export const Component = Paragraph;
