import * as React from "react";
import {
  matchPath,
  useLocation,
  Route,
  NavLink,
  Switch,
  Redirect,
} from "react-router-dom";

import { Tabs } from "react-bulma-components";
import Preview from "./Preview";
import PageBuilder from "./Builder";

export default function Builder() {
  const location = useLocation();
  const [layout, setLayout] = React.useState(
    JSON.parse(localStorage.getItem("layout")) || []
  );

  return (
    <>
      <Tabs align="center" size="medium" type="toggle-rounded">
        <Tabs.Tab
          active={matchPath(location.pathname, "/builder/builder")}
          renderAs={() => (
            <NavLink to="/builder/builder" activeClassName="is-active">
              Builder
            </NavLink>
          )}
        />
        <Tabs.Tab
          active={matchPath(location.pathname, "/builder/preview")}
          renderAs={() => (
            <NavLink to="/builder/preview" activeClassName="is-active">
              Preview
            </NavLink>
          )}
        />
      </Tabs>

      <Switch>
        <Route path="/builder/builder">
          <PageBuilder layout={layout} setLayout={setLayout} />
        </Route>

        <Route path="/builder/preview">
          <Preview layout={layout} />
        </Route>

        <Route>
          <Redirect to="/builder/builder" />
        </Route>
      </Switch>
    </>
  );
}
