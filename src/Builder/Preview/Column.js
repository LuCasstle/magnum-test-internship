import * as React from "react";

import Component from "./Component";

export default function Column({ column }) {
  const { components } = column;

  return (
    <div className="column">
      {components.map((component) => (
        <Component key={component.id} component={component} />
      ))}
    </div>
  );
}
