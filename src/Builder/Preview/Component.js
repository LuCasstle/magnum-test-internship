import * as React from "react";
import COMPONENT_MAP from "../components";

export default function Component({ component }) {
  const { componentId, options } = component;
  const ComponentModule = COMPONENT_MAP[componentId];

  return (
    <div className="component">
      <ComponentModule.Component options={options} />
    </div>
  );
}
