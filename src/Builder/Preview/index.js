import * as React from "react";

import Row from "./Row";

export default function Preview({ layout }) {
  return (
    <div className="app-content preview">
      {layout.map((row) => (
        <Row row={row} id={row.id} />
      ))}
    </div>
  );
}
