import * as React from "react";

import { Columns } from "react-bulma-components";
import Column from "./Column";
import Component from "./Component";

export default function Row({ row }) {
  return (
    <div className="row">
      {row.columns.length > 0 ? (
        <Columns>
          {row.columns.map((column) => {
            const { size } = column;

            return (
              <Columns.Column size={size} key={column.id}>
                <Column column={column} />
              </Columns.Column>
            );
          })}
        </Columns>
      ) : (
        row.components.map((component) => (
          <Component key={component.id} component={component} />
        ))
      )}
    </div>
  );
}
