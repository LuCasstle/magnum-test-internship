import * as React from "react";

import { Navbar } from "react-bulma-components";
import { NavLink } from "react-router-dom";

export default function Header() {
  return (
    <Navbar color="primary">
      <Navbar.Menu>
        <NavLink
          to="/builder/builder"
          className="navbar-item"
          activeClassName="is-active"
        >
          Builder
        </NavLink>

        <NavLink
          to="/schema"
          className="navbar-item"
          activeClassName="is-active"
        >
          Modèle de données
        </NavLink>
      </Navbar.Menu>
    </Navbar>
  );
}
