import * as React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import "./styles/index.scss";
import Header from "./Header";
import Builder from "./Builder";
import Schema from "./Schema";

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <div className="app-container">
          <Switch>
            <Route path="/schema">
              <Schema />
            </Route>
            <Route path="/builder">
              <Builder />
            </Route>
            <Route>
              <Redirect to="/builder" />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
