# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Pourquoi ce test ?

Ce test a pour objectif de tester la capacité à élaborer une solution abstraite à un problème complexe donné et à l'implémenter.
Ce test vous mettra aussi en situation par rapport au rôle qui sera le vôtre au sein de Magasin Numérique, puisque Magasin Numérique implémente une solution de création de site raccroché à un CMS (Content Management System).

A Magasin Numérique, nous administrons les sites de plusieurs clients comment Secrets d'histoire (https://secretsdhistoire.tv), Front Populaire (https://frontpopulaire.fr) et AfterFoot (https://afterfoot.media).

Ces sites sont créés à partir d'un backoffice qui permet à la fois

- la création de modèle de données
- la manipulation des données (création, édition, suppression)
- la connexion à des services externes
- la création du site à proprement parlé, constitué d'un squelette commun (pages login, signup, etc), de pages statiques de présentation (home, contacts, etc) et enfin de page dynamique (pages contenus, vidéos, etc).

## Objectifs du test

Vous allez devoir créer un micro générateur de site associé avec un visualisateur avec un modèle de données basique, mais dynamique.

## Preset

On utilise

- `create-react-app` pour lancer notre application react => https://reactjs.org/docs/create-a-new-react-app.html
- `react-bulma-components` pour les composants de base, implémentation React des composants CSS de Bulma qui est un framework css => https://bulma.io/. Vous pouvez utiliser tous les composants de Bulma
- `react-router` pour le routing
